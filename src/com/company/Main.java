package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;



public class Main {

    //Method to find the connected nodes


    static void traverseEmployee(List<EmployeeNode> node, int id) {

        for(int i=0;i<node.size();i++){

            if(node.get(i).id == id){       //Finding the employee based on the input ID

                System.out.println("\n Id :"+node.get(i).id+" \tName :"+node.get(i).Name+" \tDesignation :"+node.get(i).Designation );


                System.out.println("\n\nConnected Parents are : ");     //Finding Connected nodes

                for(int j=0;j<node.get(i).Parent.size();j++){

                    int z=node.get(i).Parent.get(j);

                    for(int q=0;q<node.size();q++) {

                        if(node.get(q).id == z){

                            System.out.println("\nId :"+node.get(q).id+" \tName :"+node.get(q).Name+" \tDesignation :"+node.get(q).Designation );
                        }
                    }
                }

            }

        }
    }


    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        final String AM = "Assistant Manager";
        final String TL = "Tech Lead";
        final String SE = "Software Engineer";

        String name;
        int id=2;
        int count = 1;

        int value;

        List<EmployeeNode> node = new ArrayList<>();
        node.add(new EmployeeNode("Raj", 1, "Manager"));   //Root node

        do

        {
            System.out.println("Welcome to employee Database\n\n");

            System.out.println("1. Insert Assistant Manager");

            System.out.println("2. Insert Tech Lead");

            System.out.println("3. Insert Software Engineer ");

            System.out.println("4. Find connected Employees\n");

            System.out.println("Enter Your Choice - ");

            int choice = scan.nextInt();

            switch (choice)

            {

                case 1:

                    scan.nextLine();

                    System.out.print("Name : ");
                    name = scan.nextLine();


                    node.add(new EmployeeNode(name, id, AM));
                    node.get(count).addAssistantManager();
                    node.get(count).displayEmployeeDetails();


                    break;

                case 2:

                    scan.nextLine();

                    System.out.print("Name : ");
                    name = scan.nextLine();


                    node.add(new EmployeeNode(name, id, TL));
                    node.get(count).addTechLead();
                    node.get(count).displayEmployeeDetails();

                    break;

                case 3:

                    scan.nextLine();

                    System.out.print("Name : ");
                    name = scan.nextLine();


                    node.add(new EmployeeNode(name, id, SE));
                    node.get(count).addSoftwareEng();
                    node.get(count).displayEmployeeDetails();


                    break;

                case 4:


                    System.out.print("Enter ID of employee: ");
                    id = scan.nextInt();

                    scan.nextLine();

                    traverseEmployee(node, id); //Function to traverse hierarchy

                    break;


                default:

                    System.out.println("Wrong Entry \n ");

                    break;

            }

            count++;
            id++;

            System.out.println("\nDo you want to continue (1=Yes or 0=No) \n");

            value = scan.nextInt();


        } while (value == 1);

    }

}
