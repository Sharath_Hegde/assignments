package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class EmployeeNode {


    String Name;
    int id;
    String Designation;

    List<Integer> Parent;
    List<Integer> Child;


    public EmployeeNode(String Name, int id, String Designation) {

        this.Name = Name;
        this.id = id;
        this.Designation = Designation;
        Parent = new ArrayList<>();
        Child = new ArrayList<>();

    }


    public void addAssistantManager( ){

        Parent.add(1);


    }

    public void addTechLead(){


        int number, ids;
        System.out.println("Enter number of Assistant Managers who are connected Tech Lead:");

        Scanner scan = new Scanner(System.in);
        number = scan.nextInt();

        System.out.println("Enter Assistant Managers Id");  //Storing links in the list

        for (int i = 0; i < number; i++) {

            ids = scan.nextInt();

            Parent.add(ids);

        }
    }


    public void addSoftwareEng() {

        int number, ids;
        System.out.println("Enter number of TechLeads who are connected to Software Engineers:");


        Scanner scan = new Scanner(System.in);
        number = scan.nextInt();


        System.out.println("Enter TechLeads Id");

        for (int i = 0; i < number; i++) {

            ids = scan.nextInt();

            Parent.add(ids);

        }
    }

    public void displayEmployeeDetails() {

        System.out.println("\nEmployee record has been added successfully");

        System.out.println("\nId :"+id+" \tName :"+Name+" \tDesignation :"+Designation );

    }
}
